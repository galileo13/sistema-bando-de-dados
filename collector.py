# put all objects here
# and them import them from routes to put in db

import os
import pandas as pd
from sys import argv
import re
import shutil
import csv

### AUXILIARY ROUTINES ###
# funcao que ordena as pastas nas pastas das galaxias processadas, pois linux quebra ordem dos Runs
def custom_sort(t):
    if t[0]=='R':
    	temp = re.search('Run(.*)_58', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def mask_sort(t):
    if t[0]=='m':
    	temp = re.search('mask(.*)_58', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def img_sort(t):
    if t[0]=='i':
    	temp = re.search('img(.*)_58', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def getIds(list):
    cleanList = []
    for item in list:
        id = re.search('_(.*)_band_r', item) 
        cleanList.append(id.group(1))
    
    return cleanList

def fixPathOrder(genericPath, status, message):
    path = []
    y = 0
    for i in range(len(status)):
        if status[i] == 'Error':
            path.append(message)
        else:
            path.append(genericPath[y])
            y = y+1
    
    return path

def filterMaskList(list):
    filteredList = []

    for file in list:
        if file.endswith('_r.fits'):
            filteredList.append(file)
    return filteredList

### MAIN ROUTINES ####
def getProcessedFolders(path, startsWith):
    items = os.listdir(path)

    newlist = []
    for names in items:
        if names.startswith(startsWith):
            newlist.append(names)

    return newlist

def getProcessingTime(path):
    path = path + '/runTime'
    if os.path.isfile(path):
        f = open(path, "r")
        lines = f.readlines()
        time = lines[2]
        f.close()
    else:
        time = 'No Info'

    return time

def getOkayGalaxies(path, modelPath, folders, containingFolder):
    okayGalaxies = []
    errorGalaxies = []
    posteriors = []
    cornerPlots = []
    foldersPath = []
    for folder in folders:
        galaxyPath = path+modelPath+folder
        # getting only processed folder and galaxy folder itself, full path will be supplied later
        a, b = galaxyPath.split(containingFolder)
        foldersPath.append(b)
        for file in os.listdir(galaxyPath):
            if file.endswith('fits.gz'):
                okayGalaxies.append(galaxyPath)
                posteriors.append(galaxyPath+'/'+file)
            elif file.endswith('.png'):
                cornerPlots.append(galaxyPath+'/'+file)
        if galaxyPath not in okayGalaxies:
            errorGalaxies.append(galaxyPath)
    
    return okayGalaxies, errorGalaxies, posteriors, cornerPlots, foldersPath

def getErrorDescription(galaxyList, galaxyOkayList, errorListPath):
    errorDescriptionList = []
    logRunErroList = []
    errors = []
    if os.path.isfile(errorListPath):
        #data = pd.read_csv(errorListPath, header=None)
        with open(errorListPath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                logRunErroList.append(row[2])
                errors.append(row[-1])

    errorCounter = 0
    for i in range(len(galaxyList)):
        if galaxyList[i] in logRunErroList:
            errorDescriptionList.append(errors[errorCounter])
            errorCounter=errorCounter+1
        elif galaxyList[i] in galaxyOkayList:
            errorDescriptionList.append('No Error')
        else:
            errorDescriptionList.append('Strange Error')
    

    return errorDescriptionList


def collectObjects(objectFinalPath, objectsList, perentList, endingName):
    genericPath = []
    i = 0
    for object in objectsList:
        if not os.path.isfile(objectFinalPath+perentList[i]+endingName):
            shutil.copy(object, objectFinalPath+perentList[i]+endingName)

        genericPath.append(objectFinalPath+perentList[i]+endingName)
        i=i+1
    
    return genericPath

def getImagesAndMask(path, ids, imgsFinalPath, masksFinalPath):
    imgPath = []
    maskPath = []
    # move it to img and mask folder respectevely
    imgs = getProcessedFolders(path, 'img')
    imgs.sort(key=img_sort)

    masks = getProcessedFolders(path, 'mask')
    masks.sort(key=mask_sort)
    masks = filterMaskList(masks)
    
    for i in range(len(ids)):
        shutil.copy(path+imgs[i], imgsFinalPath+ids[i]+'.fits')
        shutil.copy(path+masks[i], masksFinalPath+ids[i]+'.fits')
        
        imgPath.append(imgsFinalPath+ids[i]+'.fits')
        maskPath.append(masksFinalPath+ids[i]+'.fits')
    
    return imgPath, maskPath

def getProcessingHost(galaxyList, okListPath):

    if os.path.isfile(okListPath):
        #data = pd.read_csv(errorListPath, header=None)
        with open(okListPath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            # need only first row, cus all following are the same in the same folder
            for row in csv_reader:
                host = row[6]
                break
    else:
        host = 'Unknown'

    # just filling list with same host name for the whole list
    processingHostList = [host] * len(galaxyList)
    
    return processingHostList
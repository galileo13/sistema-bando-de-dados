from models import db, Galaxy, ProcessedSersicPlusExponential, ProcessedSersic, GalaxySchema, ProcessedSersicSchema, ProcessedSersicPlusExponentialSchema
import os
import collector as collect
from sys import argv
import pandas as pd

"""
DB Filler (python3)
Example
python main.py /Users/igor/GALPHAT/priorTests/processed1/ SersicPlusExp containginFolder

where: 
first argument is a path containing output folder
second argument is model
third argument is folder containing all the processed folders
"""
def upgradeDB(model, dataComplete):
    db.create_all()
    
    galaxySchema = GalaxySchema()
    processedSersicSchema = ProcessedSersicSchema()
    processedSersicPlusExponentialSchema= ProcessedSersicPlusExponentialSchema()
    
    # check if galaxy already in galaxy list
    galaxies = list(db.session.query(Galaxy.objid).all())
    galaxies = [r[0] for r in galaxies] # converting to list

    # check if galaxy already in processed galaxy list
    if model == 'Sersic':
        processedGalaxies = list(db.session.query(ProcessedSersic.galaxyId).all())
        processedGalaxies = [r[0] for r in processedGalaxies] # converting to list
    elif model == 'SersicPlusExp':
        processedGalaxies = list(db.session.query(ProcessedSersicPlusExponential.galaxyId).all())
        processedGalaxies = [r[0] for r in processedGalaxies] # converting to list

    
    data = dataComplete
    for i in range(len(data['objid'])):
        if not str(data['objid'][i]) in galaxies:
            galaxy = Galaxy(band=data['band'][i],
                            objid=str(data['objid'][i]),
                            mag=data['Mag'][i],
                            rePix=data['re_pix'][i],
                            n=data['n'][i],
                            q=data['q'][i],
                            pa=data['pa'][i],
                            sky=data['Sky'][i],
                            imageSize=int(data['imagenSize'][i]),
                            zeroPoint=data['zeroPoint'][i],
                            snr=data['SNR'][i],
                            petroRadpix=data['petroRadpix'][i],
                            petroMag=data['petroMag'][i],
                            devRad=data['deVRad'][i],
                            deVAB=data['deVPhi'][i],
                            type=data['type'][i],
                            imgPath=data['img'][i],
                            maskPath=data['mask'][i]
                            )
            db.session.add(galaxy)
        
        if model == 'Sersic':
            processedGalaxy = ProcessedSersic(
                galaxyId = str(data['objid'][i]),
                processingStatus = str(data['ProcessingStatus'][i]),
                processingTime = str(data['ProcessingTime'][i]),
                processingError = str(data['ProcessingError'][i]),
                posterior = str(data['PosterioPath'][i]),
                cornerPlot = str(data['CornerPlot'][i]),
                processingHost = str(data['host'][i]),
                processingFolder = str(data['processingFolder'][i])
            )
        elif model == 'SersicPlusExp':
            processedGalaxy = ProcessedSersicPlusExponential(
                galaxyId = str(data['objid'][i]),
                processingStatus = str(data['ProcessingStatus'][i]),
                processingTime = str(data['ProcessingTime'][i]),
                processingError = str(data['ProcessingError'][i]),
                posterior = str(data['PosterioPath'][i]),
                cornerPlot = str(data['CornerPlot'][i]),
                processingHost = str(data['host'][i]),
                processingFolder = str(data['processingFolder'][i])
            )
        
        # updating entries
        if model == 'Sersic':
            if not str(data['objid'][i]) in processedGalaxies:
                db.session.add(processedGalaxy)
            elif data['ProcessingStatus'][i] == 'Success':
                print('Updating the entry of ', i, data['objid'][i])
                a_galaxy = db.session.query(ProcessedSersic).filter(ProcessedSersic.galaxyId == str(data['objid'][i])).one()
                a_galaxy.processingStatus = str(data['ProcessingStatus'][i])
                a_galaxy.processingTime = str(data['ProcessingTime'][i])
                a_galaxy.processingError = str(data['ProcessingError'][i])
                a_galaxy.posterior = str(data['PosterioPath'][i])
                a_galaxy.cornerPlot = str(data['CornerPlot'][i])
                a_galaxy.processingHost = str(data['host'][i])
                a_galaxy.processingFolder = str(data['processingFolder'][i])
        elif model == 'SersicPlusExp':
            if not str(data['objid'][i]) in processedGalaxies:
                db.session.add(processedGalaxy)
            elif data['ProcessingStatus'][i] == 'Success':
                print('Updating the entry of ', i, data['objid'][i])
                a_galaxy = db.session.query(ProcessedSersic).filter(ProcessedSersicPlusExponential.galaxyId == str(data['objid'][i])).one()
                a_galaxy.processingStatus = str(data['ProcessingStatus'][i])
                a_galaxy.processingTime = str(data['ProcessingTime'][i])
                a_galaxy.processingError = str(data['ProcessingError'][i])
                a_galaxy.posterior = str(data['PosterioPath'][i])
                a_galaxy.cornerPlot = str(data['CornerPlot'][i])
                a_galaxy.processingHost = str(data['host'][i])
                a_galaxy.processingFolder = str(data['processingFolder'][i])

    db.session.commit()

path = argv[1]
model = argv[2]
containingFolder = argv[3]


basedir = os.path.abspath(os.path.dirname(__file__))
sersicOutput = 'outputGalphatSersic/'
sersicPlusExpOutput = 'outputGalphatSersicPlusExp/'
auxFilesPath = 'inputGalphat/fits/'

posteriorPath = 'posteriors/'
cornerPlotsPath = 'cornerPlots/'

imgsPath = 'imgs/'
masksPath = 'masks/'

# posteriorPath = 'posteriorsBench/'
# cornerPlotsPath = 'cornerPlotsBench/'

# imgsPath = 'imgsBench/'
# masksPath = 'masksBench/'


# listComplete
listCompletePath = path + 'listComplete'
data = pd.read_csv(listCompletePath)

# get processing folders (Runs)
if model == 'Sersic':
    processedFolders = collect.getProcessedFolders(path+sersicOutput, 'Run')
    processedFolders.sort(key=collect.custom_sort) # ordena de Run1 ate RunN-1
elif model == 'SersicPlusExp':
    processedFolders = collect.getProcessedFolders(path+sersicPlusExpOutput, 'Run')
    processedFolders.sort(key=collect.custom_sort) # ordena de Run1 ate RunN-1

print("Number of galaxies = ", len(processedFolders))

processingTimeList = []    
if model == 'Sersic':
    for folder in processedFolders:
        processingTimeList.append(collect.getProcessingTime(path+sersicOutput+folder))
elif model == 'SersicPlusExp':
    for folder in processedFolders:
        processingTimeList.append(collect.getProcessingTime(path+sersicPlusExpOutput+folder))

if model == 'Sersic':
    okayGalaxies, errorGalaxies, posteriors, cornerPlots, foldersPath = collect.getOkayGalaxies(path, sersicOutput, processedFolders, containingFolder)
elif model == 'SersicPlusExp':
    okayGalaxies, errorGalaxies, posteriors, cornerPlots, foldersPath = collect.getOkayGalaxies(path, sersicPlusExpOutput, processedFolders, containingFolder)


allCleal = collect.getIds(processedFolders)
cleanErrors = collect.getIds(errorGalaxies)
cleanOkays = collect.getIds(okayGalaxies)

# getting errror
if model == 'Sersic':
    errorDescriptionList = collect.getErrorDescription(allCleal, cleanOkays, path+sersicOutput+'logRunErro')
    pass
elif model == 'SersicPlusExp':
    errorDescriptionList = collect.getErrorDescription(allCleal, cleanOkays, path+sersicPlusExpOutput+'logRunErro')

# getting processing host
if model == 'Sersic':
    processingHostList = collect.getProcessingHost(allCleal, path+sersicOutput+'logRunOk')
elif model == 'SersicPlusExp':
    processingHostList = collect.getProcessingHost(allCleal, path+sersicPlusExpOutput+'logRunOk')

genericPosteriorPath = collect.collectObjects(posteriorPath, posteriors, cleanOkays, '.fits.gz')
genericCornerPath = collect.collectObjects(cornerPlotsPath, cornerPlots, cleanOkays, '.png')

genericImgPath, genericMaskPath = collect.getImagesAndMask(path+auxFilesPath, allCleal, imgsPath, masksPath)

status = []
for galaxy in allCleal:
    if galaxy in cleanErrors:
        status.append('Error')
    elif galaxy in cleanOkays:
        status.append('Success')

posteriorPath = collect.fixPathOrder(genericPosteriorPath, status, 'No Posterior')
cornerPath = collect.fixPathOrder(genericCornerPath, status, 'No Corner Plot')
#cornerPath = ['No Corner Plot'] * len(processedFolders)

statusColumn = pd.DataFrame({'ProcessingStatus': status})
processingTimeColumn = pd.DataFrame({'ProcessingTime': processingTimeList})
processingErrorColumn  = pd.DataFrame({'ProcessingError': errorDescriptionList})
posteriorsColumn = pd.DataFrame({'PosterioPath': posteriorPath})
cornerPlotColumn = pd.DataFrame({'CornerPlot': cornerPath})
imgColumn = pd.DataFrame({'img': genericImgPath})
maskColumn = pd.DataFrame({'mask': genericMaskPath})

hostColumn = pd.DataFrame({'host': processingHostList})
folderComlumn = pd.DataFrame({'processingFolder': foldersPath})

dataComplete = pd.concat([data, statusColumn, processingTimeColumn, processingErrorColumn, posteriorsColumn, cornerPlotColumn, imgColumn, maskColumn, hostColumn, folderComlumn], axis=1)
dataComplete = dataComplete[:len(allCleal)]
print(dataComplete)

upgradeDB(model, dataComplete)
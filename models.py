from flask_sqlalchemy import SQLAlchemy
import os
from flask import Flask
from flask_marshmallow import Marshmallow

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Galaxy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    band = db.Column(db.String(3))
    objid = db.Column(db.String(18), unique=True, index=True)
    mag = db.Column(db.Float)
    rePix = db.Column(db.Float)
    n = db.Column(db.Float)
    q = db.Column(db.Float)
    pa = db.Column(db.Float)
    sky = db.Column(db.Float)
    imageSize = db.Column(db.Integer)
    zeroPoint = db.Column(db.Float)
    snr = db.Column(db.Float)
    petroRadpix = db.Column(db.Float)
    petroMag = db.Column(db.Float)
    devRad = db.Column(db.Float)
    deVAB = db.Column(db.Float)
    type = db.Column(db.String(10))
    imgPath = db.Column(db.String)
    maskPath = db.Column(db.String)

    def __repr__(self):
        return '<Galaxy {}>'.format(self.objid)

class ProcessedSersicPlusExponential(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    galaxyId = db.Column(db.ForeignKey('galaxy.objid'), unique=True, index=True)
    galaxy = db.relationship("Galaxy", backref=db.backref("galaxySersicexponential", uselist=False))
    processingTime = db.Column(db.String)
    processingStatus = db.Column(db.String)
    processingError = db.Column(db.String)
    posterior = db.Column(db.String)
    cornerPlot = db.Column(db.String)
    processingHost = db.Column(db.String)
    processingFolder = db.Column(db.String)

    def __repr__(self):
        return '<Galaxy {}>'.format(self.galaxyId)

class ProcessedSersic(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    galaxyId = db.Column(db.ForeignKey('galaxy.objid'), unique=True, index=True)
    galaxy = db.relationship("Galaxy", backref=db.backref("galaxySersic", uselist=False))
    processingTime = db.Column(db.String)
    processingStatus = db.Column(db.String)
    processingError = db.Column(db.String)
    posterior = db.Column(db.String)
    cornerPlot = db.Column(db.String)
    processingHost = db.Column(db.String)
    processingFolder = db.Column(db.String)
    
    def __repr__(self):
        return '<Galaxy {}>'.format(self.galaxyId)

# serialization part
class GalaxySchema(ma.ModelSchema):
    class Meta:
       model = Galaxy

class ProcessedSersicPlusExponentialSchema(ma.ModelSchema):
    galaxy = ma.Nested(GalaxySchema)
    class Meta:
       model = ProcessedSersicPlusExponential

class ProcessedSersicSchema(ma.ModelSchema):
    galaxy = ma.Nested(GalaxySchema)    
    class Meta:
       model = ProcessedSersic